const express = require('express');
const app = express();
const PORT = 3000;
const mongoose = require('mongoose');

//middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect('mongodb+srv://jrc4827:admin@batch139.glw5n.mongodb.net/myFirstDatabase?retryWrites=true&w=majority', 
	{useNewUrlParser: true, useUnifiedTopology: true}
);

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log(`Connected to database`));

//Schema
const userSchema = new mongoose.Schema(
	{
		username: String,
        password: String,
    } 
);
 
//Models
const User = mongoose.model("User", userSchema);

app.post("/signup", (req, res) => {

	User.findOne({username: req.body.username}, (err, result) => {

		if(result != null && result.username == req.body.username){
			return res.send("A user has already registered with this username")
			
		} else {
			let newUser = new User({
				username: req.body.username,
                password: req.body.password,
			})

			newUser.save((err, savedUser) => {
				if(err){
					return console.error(err)
				} else {
					return res.send(savedUser)
				}
			})
		}
	})	
})

app.listen(PORT, () => console.log(`Server running at port ${PORT}`));


